﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    public static class Challenge20
    {
        /*
         * Challenge - Datatypes And Variables || Section 2.20
         * Create a variable for each of the primitive datatypes (https://docs.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/types-and-variables).
         * Leave the Object datatype out. 
         * Initialize each variable with a working value.
         * Then create two values of type string. 
         * The first one should say "I control text"
         * The second one should be a whole number. Then use the Parse method in order to convert that string to an integer.
         * Add each an output for each of the variables and write it onto the console. (WriteLine)
         */

        // Integral Types

        static sbyte _sbyte = 127;
        static byte _byte = 255;
        static short _short = 32767;
        static ushort _ushort = 65535;
        static int _myint = 2147483647;
        static uint _uint = 4294967295;
        static long _long = 9223372036854775807;
        static ulong _ulong = 18446744073709551615;

        // Floating-point Types

        public const float _myPiFloat = (float)_myPiDecimal;
        public const double _myPiDouble = (double)_myPiDecimal;
        public const decimal _myPiDecimal = 3.1415926535897932384626433832795028M;

        // Bool

        static bool _myBool = true;

        // String & Char

        static char _myChar = 'A';
        static string _myString = "I control text";
        static string _myString2 = "25";
        static int _myStringToInt = Int32.Parse(_myString2);

        public static void PrintEverything()
        {
            Console.WriteLine($"sByte: {_sbyte}");
            Console.WriteLine($"Byte: {_byte}");
            Console.WriteLine($"Short: {_short}");
            Console.WriteLine($"uShort: {_ushort}");
            Console.WriteLine($"Int: {_myint}");
            Console.WriteLine($"uInt: {_uint}");
            Console.WriteLine($"Long: {_long}");
            Console.WriteLine($"uLong: {_ulong}");
            Console.WriteLine($"Float: {_myPiFloat}");
            Console.WriteLine($"Double: {_myPiDouble}");
            Console.WriteLine($"Decimal: {_myPiDecimal}");
            Console.WriteLine($"Bool: {_myBool}");
            Console.WriteLine($"Char: {_myChar}");
            Console.WriteLine($"String: {_myString}");
            Console.WriteLine($"String2: {_myString2}");
            Console.WriteLine($"StringToInt: {_myStringToInt}");

        }

    }

}
