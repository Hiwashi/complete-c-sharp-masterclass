﻿using System; // Allows to use the functionality inside of the System NameSpace.
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// FROM LESSON 1 TO 19!

/* Naming Conventions - Class 17 // https://www.dofactory.com/reference/csharp-coding-standards
 * Try nouns or noun phrases to name Classes.             * 
 * Class Names and Methodo should use Pascal Cassing, e.g. ClientActiviy, MyClassName, AnotherExemple.
 * Method Arguments and Local Variables use Cammel Casing, e.g. firstNumber, thisArgument, itemCount.
 * AVOID ABREVIATIONS! Use "userControl" instead of "usrCtrl" ( Exceptions: XML, FTP, etc ).
 * DONT USE NUMBERS AT THE START OF VARIABLES. 
 * ONLY USE UNDERSCORE(_) AT THE BEGINNING! So use _playerName, NOT player_name or player_Name.
 * AVOID CAPITAL String, Int32 and Boolean, instead use string, int, bool.
 */

namespace HelloWorld // New Namespace
{
    class Program
    {
        // Constants are immutable values which are known at compile time and do not change for the duration of the program
        // Constant as fields

        const double PI = Math.PI;
        const int weeks = 52, months = 12;
        const string _myBrithday = "06/11/1823";

        static void Main(string[] args)
        {
            // More data type info: https://docs.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/types-and-variables

            int num1 = 13;
            int num2 = 5;
            int sum = num1 + num2;

            float f1 = 3.5f; // Must use "f" or "F" at the end of a float
            float f2 = 1.337f;
            float divF = f1 / f2;

            double d1 = 3.5d; // Must use "d" or "D" at the end of a double
            double d2 = 1.337d;
            double divD = d1 / d2;

            decimal deci1 = 3.5M; // Must use "m" or "M" at the end of a decimal
            decimal deci2 = 1.337M;
            decimal divDeci = deci1 / deci2;

            Console.WriteLine($"{num1} + {num2} = {sum}");
            Console.WriteLine($"{f1} / {f2} = {divF}");
            Console.WriteLine($"{d1} / {d2} = {divD}");
            Console.WriteLine($"{deci1} / {deci2} = {divDeci}");

            Console.WriteLine("\n");
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("\n");

            string message = "My name is";
            string myName = "Hiwashi";
            string capsMessage = message.ToUpper();
            string lowerCaseMessage = message.ToLower();

            Console.WriteLine($"{message} {myName}");
            Console.WriteLine($"{capsMessage} {myName}");
            Console.WriteLine($"{lowerCaseMessage} {myName}");

            Console.WriteLine("\n");
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("\n");

            // 18.Implicit and Explicit Conversion

            /* IMPLICIT CONVERSION
             * No special syntax is required because the conversion is type safe and no data will be lost. 
             * Examples include conversions from smaller to larger integral types, and conversions from derived classes to base classes.
             */

            int num = 12390532;
            long bigNum = num;

            float myFloat = 13.37F;
            double myNewDouble = myFloat;

            /* EXPLICIT CONVERSION (CASTS)
             * Explicit conversions require a cast operator. Casting is required when information might be lost in the conversion, 
             * or when the conversion might not succeed for other reasons. Typical examples include numeric conversion to a type that
             * has less precision or a smaller range, and conversion of a base-class instance to a derived class.
             */
            double myDouble = 13.37;
            int myInt;

            myInt = (int)myDouble;


            // TYPE CONVERSION!
            string myString = myDouble.ToString(); // "13,37"
            string myFloatString = myFloat.ToString();
            bool sunIsShining = true;

            Console.WriteLine(sunIsShining.ToString());

            Console.WriteLine("\n");
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("\n");

            // How to parse

            string myString1 = "15";
            string myString2 = "13";
            string myResultIs = myString1 + myString2;
            int number1 = Int32.Parse(myString1);
            int number2 = Int32.Parse(myString2);
            int myResult = number1 + number2;

            Console.WriteLine(myResultIs); // INCORRECT

            Console.WriteLine(myResult); // CORRECT

            char _myChar = 'A';
            string _myString = "I control text";
            string _myString2 = "25";

            int _myStringToInt = Int32.Parse(_myString2);

            Console.WriteLine("\n");
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("First Challenge!");
            Console.WriteLine("\n");

            Challenge20.PrintEverything();


            Console.WriteLine("\n");
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("\n");

            Console.WriteLine($"My birthday is always going to be: {_myBrithday}");

            // Cheat Sheet
            // https://www.cheatography.com/laurence/cheat-sheets/c/
            // https://www.thecodingguys.net/resources/cs-cheat-sheet.pdf







            Console.Read();
        }
    }
}
