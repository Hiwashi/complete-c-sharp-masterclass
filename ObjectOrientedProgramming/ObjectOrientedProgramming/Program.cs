﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgramming
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create an object of my class
            // an instance of Creature
            Servant Scathach = new Servant("1", "Scáthach", "Lancer", 8, 2, 80, 20);
            Scathach.AllStats();
            Servant Gilgamesh = new Servant("2", "Gilgamesh", "Archer", 9, 2, 90, 30);
            Gilgamesh.AllStats();
        }
    }
} 
