﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgramming
{
    class Servant
    {
        // https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/properties

        private string ID { get; set; }
        private string Name { get; set; }
        private string Class { get; set; }
        private int Attack { get; set; }
        private int Defense { get; set; }
        private int Health { get; set; }
        private int Mana { get; set; }

        public Servant(string _id, string _name, string _class, int _attack, int _defense, int _health, int _mana)
        {
            ID = _id;
            Name = _name;
            Class = _class;
            Attack = _attack;
            Defense = _defense;
            Health = _health;
            Mana = _mana;
        }
        public void AllStats()
        {
            Console.WriteLine($"ID: {ID}");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Class: {Class}");
            Console.WriteLine($"Attack: {Attack}");
            Console.WriteLine($"Defense: {Defense}");
            Console.WriteLine($"Health: {Health}");
            Console.WriteLine($"Mana: {Mana}");
            Console.WriteLine("\n");

        }
    }
}
