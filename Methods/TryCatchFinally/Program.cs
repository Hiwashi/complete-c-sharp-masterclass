﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchFinally
{
    class Program
    {
        static void Main(string[] args)
        {
            // TRY, CATCH AND FINALLY

            Console.WriteLine("Please enter a number:");
            string userInput = Console.ReadLine();

            // TRY THE BELOW CODE, IF IT DOESN'T WORK THEN I DON'T WANT THE PROGRAM TO CRASH

            try
            {
                int userInputAsInt = int.Parse(userInput);
            }

            // IF IT DOESN'T WORK, IT WILL EXECUTE THE CODE INSIDE THE CATCH BODY

            catch (ArgumentNullException) // Mouseover Parse to see possible exceptions!
            {
                Console.WriteLine("ArgumentNullException, the value was empty(null)");
            }
            catch (FormatException)
            {
                Console.WriteLine("FormatException, please enter the correct datatype!");
            }
            catch (OverflowException)
            {
                Console.WriteLine("OverflowException, the number was too long or too short for this data type.");
            }
            finally // Code to be execute whenever Try and/or catch are done.
            {
                Console.WriteLine("This is called anyways");
            }

            // Challenge: Divide by 0

            Console.WriteLine("\nLet's try to divide by 0! What could go wrong?");
            Console.WriteLine("Please enter the number you want to divide by 0");
            int _inputNumber = int.Parse(Console.ReadLine());
            try
            {
                Console.WriteLine($"{_inputNumber} / 0 = {_inputNumber / 0}");

            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("You cannot divide by 0!");                
            }
            catch (Exception)
            {
                Console.WriteLine("Something else went wrong, please panic");
            }
            finally
            {
                Console.WriteLine("Finally we execute \"finally{}\"");
            }


            Console.ReadKey();
        }
    }
}
