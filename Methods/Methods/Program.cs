﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteSomething();
            WriteSomethingSpecific("I am an argument and am called from a method");
            Console.WriteLine("\n");

            int num1 = 7;
            int num2 = 34;

            Console.WriteLine($"{num1} + {num2} = {Add(num1, num2)}");
            Console.WriteLine($"{num1} - {num2} = {Sub(num1, num2)}");
            Console.WriteLine($"{num1} * {num2} = {Multiply(num1, num2)}");
            Console.WriteLine($"{num1} / {num2} = {Divide(num1, num2)}");
            Console.WriteLine("\n");

            // Complicated way to do it but to keep Challenges on different classes.
            // REMEMBER THAT TO DO THIS WITHOUT HAVING TO CREATE A NEW INSTANCE OF AN OBJECT ( Object Variable = new Object(); )
            // EVERYTHING ON THE CLASS MUST BE STATIC.

            Console.WriteLine(Challenge29.GreetFriend(Challenge29.friend1));
            Console.WriteLine(Challenge29.GreetFriend(Challenge29.friend2));
            Console.WriteLine(Challenge29.GreetFriend(Challenge29.friend3));
            Console.WriteLine("\n");


            // GETTING USER INPUT TO USE ON METHODS

            Console.WriteLine("Enter the first number: ");
            int inputNum1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the second number: ");
            int inputNum2 = int.Parse(Console.ReadLine());

            // PASS THE USER INPUT AS PARAMETERS IN THE METHODS BELOW

            Console.WriteLine($"{inputNum1} + {inputNum2} = {Add(inputNum1, inputNum2)}");
            Console.WriteLine($"{inputNum1} - {inputNum2} = {Sub(inputNum1, inputNum2)}");
            Console.WriteLine($"{inputNum1} * {inputNum2} = {Multiply(inputNum1, inputNum2)}");
            Console.WriteLine($"{inputNum1} / {inputNum2} = {Divide(inputNum1, inputNum2)}");
            Console.WriteLine("\n");






            Console.Read();


        }

        // Method Syntax
        // <Access Specifier> (static) <ReturnType> <Method Name>(Parameter List){ Method Body }
        // ### TO CALL A METHOD WITHIN A STATIC METHOD, THE METHOD ITSELF HAS TO BE STATIC ###
        public static void WriteSomething()
        {
            Console.WriteLine("I am called from a method");
        }

        public static void WriteSomethingSpecific(string myText)
        {
            Console.WriteLine(myText);

        }

        // Methods that return a type ( int, float, string, etc )

        public static int Add(int num1, int num2)
        {
            return num1 + num2;
        }
        public static int Sub(int num1, int num2)
        {
            return num1 - num2;
        }
        public static int Multiply(int num1, int num2)
        {
            return num1 * num2;
        }
        public static double Divide(double num1, double num2)
        {
            return num1 / num2;
        }
    }
}
