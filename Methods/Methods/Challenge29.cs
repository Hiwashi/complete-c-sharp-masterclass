﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    static class Challenge29
    {
        /* Create three variables with the names of your friends.
         * Create a Method "GreetFriend" which writes something like: "Hi Frank,  my friend!" on the console, once it is called.
         * Where "Frank" should be replaced with the Name behind the argument given to the Method when it's called.
         * Greet all your three friends
         */

        public static string friend1 = "Hiwashi";
        public static string friend2 = "NearNihil";
        public static string friend3 = "Steve";

        public static string GreetFriend(string friendName)
        {
            return $"Hi {friendName}, my friend!";
        }
    }
}
