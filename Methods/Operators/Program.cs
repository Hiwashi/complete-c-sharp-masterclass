﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operators // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/
{
    class Program
    {
        static void Main(string[] args)
        {
            double num1 = 5;
            double num2 = 3;
            double num3;

            // UNARY OPERATORS

            num3 = -num1; // mutiply by -1

            Console.WriteLine($"num3 is {num3}");

            bool isSunny = true;
            Console.WriteLine($"Is it sunny? {!isSunny}"); /* The ! operator computes logical negation of its operand. 
                                                              That is, it produces true, if the operand evaluates to false, and false, 
                                                              if the operand evaluates to true: */

            // Increment Operators

            int num = 0;
            num++;

            Console.WriteLine($"num is {num}");
            Console.WriteLine($"num is {num++}");
            Console.WriteLine($"num is {++num}");

            // Decrement Operator

            num--;
            Console.WriteLine($"num is {num}");
            Console.WriteLine($"num is {num--}");
            Console.WriteLine($"num is {--num}");


            double result;

            result = num1 + num2;
            Console.WriteLine($"Result of {num1} + {num2} = {result}");

            result = num1 - num2;
            Console.WriteLine($"Result of {num1} - {num2} = {result}");

            result = num1 / num2;
            Console.WriteLine($"Result of {num1} / {num2} = {result}");

            result = num1 % num2;
            Console.WriteLine($"Result of {num1} % {num2} = {result}");

            result = num1 * num2;
            Console.WriteLine($"Result of {num1} * {num2} = {result}");

            // Relational and Type Operators

            bool isLower;

            isLower = num1 < num2;
            Console.WriteLine($"Result of {num1} < {num2} is {isLower}");
            isLower = num1 > num2;
            Console.WriteLine($"Result of {num1} > {num2} is {isLower}");

            bool isEqual;

            isEqual = num1 == num2;
            Console.WriteLine($"Is {num1} == to {num2}? {isEqual}");
            isEqual = num1 != num2;
            Console.WriteLine($"Is {num1} != to {num2}? {isEqual}");

            // Conditional Operators

            bool isLowerAndSunny;
            isLowerAndSunny = isLower && isSunny;
            Console.WriteLine($"IsLower && isSunny? {isLowerAndSunny}");

            isLowerAndSunny = isLower || isSunny;
            Console.WriteLine($"IsLower || isSunny? {isLowerAndSunny}");

            // && -> AND
            // || -> OR




            Console.ReadKey();

        }
    }
}
