﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            while (n < 5)
            {
                Console.WriteLine(n);
                n++;
            }
        }
    }
}
