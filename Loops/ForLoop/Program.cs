﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 0; i < 10; i++)
            //{
            //    Console.WriteLine($"i = {i}");
            //}
            //Console.WriteLine("End");

            // Challenge - Create a for loop that that only print the odd numbers from 0 to 20.

            for (int i = 0; i <= 20; i++) // my solution
            {
                if (i % 2 != 0)
                {
                    Console.WriteLine(i);
                }
            }

            Console.WriteLine("\n");

            for (int i = 1; i < 20; i+=2) // udemy solution
            {
                Console.WriteLine(i);
            }

        }
    }
}
