﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Create a program for a teacher that calculates the average score of his students.
 * He will enter each score individually and then get the final average score once he enters -1.
 * The tool should check if the entry is a number and should add that to the sum.
 * The numbers entered should only be between 0 and 10.
 * Finally once he is done entering scores, the program should write onto the console what the average score is.
 * Make sure the program doesn't crash if the teacher enters an incorrect value.
 */

namespace Loops
{
    class Program
    {
        static int _studentCount, _studentScoreTotal;
        static double _studentAverageScore;
        static bool _ContinueLoop = true;

        static void Main(string[] args)
        {
            Console.WriteLine("Student Score Average Calculator\n");

            while (_ContinueLoop)
            {
                Console.Write("Student Score: ");
                bool _isInputValid = int.TryParse(Console.ReadLine(), out int result);

                if (_isInputValid)
                {
                    switch (result)
                    {
                        case -1:
                            _ContinueLoop = false; // Stop loop
                            Console.WriteLine($"\nFinal Average Score is {String.Format("{0:0.##}", _studentAverageScore)}");
                            break;
                        case var _ when result < 0 || result > 10:
                            Console.WriteLine("\nMinimum score is 0, Maximum score is 10. Try again.\n");
                            break;
                        default:
                            AverageScore(result);
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("\nInvalid Format\n");
                }
            }

            //do
            //{
            //    Console.Write("Student Score: ");
            //    bool isFormatValid = int.TryParse(Console.ReadLine(), out int result);
            //    if (isFormatValid)
            //    {
            //        if (result == -1)
            //        {
            //            Console.WriteLine($"\nFinal Average Score is {String.Format("{0:0.##}", _studentAverageScore)}");
            //            _ContinueLoop = false;
            //        }
            //        else if (result >= 0 && result <= 10)
            //        {
            //            AverageScore(result);
            //        }
            //        else
            //        {
            //            Console.WriteLine("\nInvalid Score\n");
            //        }

            //    }
            //    else
            //    {
            //        Console.WriteLine("\nScore should only have numbers\n");
            //    }


            //} while (_ContinueLoop);

            Console.Read();
        }

        static void AverageScore(int studentScore)
        {
            _studentCount++;
            _studentScoreTotal += studentScore;
            _studentAverageScore = (double)_studentScoreTotal / (double)_studentCount;

            /* testing stuff 
            Console.WriteLine($"\nStudent Count: {_studentCount}");
            Console.WriteLine($"Total Score: {_studentScoreTotal}");
            Console.WriteLine($"Score Average: {_studentAverageScore}\n"); 
            */

        }
    }
}
