﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int lenghtOfText = 0;
            string wholeText = "";
            do
            {
                Console.Write("Please enter the name of a friend: ");
                string nameofAFriend = Console.ReadLine();
                int currentLenght = nameofAFriend.Length;
                lenghtOfText += currentLenght;
                wholeText += nameofAFriend;
            } while (lenghtOfText < 20);
            Console.WriteLine($"Thanks, that was enough. {wholeText}");
            Console.Read();
        }
    }
}
