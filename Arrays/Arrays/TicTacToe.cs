﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class TicTacToe
    {
        public string BoardPosition { get; set; }
        public string Display { get; set; }
        public int Value { get; set; }

        public bool Used { get; set; }


        public TicTacToe(int display, int boardPosition, int value, bool used)
        {
            this.BoardPosition = boardPosition.ToString();
            this.Display = display.ToString();
            this.Value = value;
            this.Used = used;
        }
    }
}

