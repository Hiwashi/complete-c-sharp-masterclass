﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program // Tic Tac Toe Project
    {
        static TicTacToe[,] TicTacToeBoard = new TicTacToe[3, 3];
        static int roundCount = 0;
        static bool GameOver = false;
        static bool Player = true;

        static void Main(string[] args)
        {
            InitializeArray(); // Works fine.
            Introduction(); // Skip for Testing
            GameStart();
            GameEnd(); // TIE/WINNER DOESN'T WORK, I DONT GIVE A FUCK
            Console.WriteLine("\n\nPress any key to close");
            Console.ReadKey()

        }

        static void GameStart()
        {
            do
            {
                if (Player) // True, so Player 1 - X turn.
                {
                    bool _invalidPlay = true;
                    do
                    {

                        UserBoard();
                        Console.Write("Where in the board do you wish to play Player 1 (X)? Type the number: ");
                        ConsoleKeyInfo _player1Input = Console.ReadKey();

                        if (char.IsDigit(_player1Input.KeyChar))
                        {
                            if (_player1Input.KeyChar.ToString() != "0")
                            {
                                for (int x = 0; x < 3; x++)
                                {
                                    for (int y = 0; y < 3; y++)
                                    {
                                        if (TicTacToeBoard[x, y].BoardPosition == _player1Input.KeyChar.ToString() && TicTacToeBoard[x, y].Used == true)
                                        {
                                            Console.Clear();
                                            Console.WriteLine($"Someone already played on the \"{_player1Input.KeyChar}\" board location.");
                                        }
                                        else if (TicTacToeBoard[x, y].BoardPosition == _player1Input.KeyChar.ToString() && TicTacToeBoard[x, y].Used == false)
                                        {
                                            TicTacToeBoard[x, y].Display = "X";
                                            TicTacToeBoard[x, y].Value = 1;
                                            TicTacToeBoard[x, y].Used = true;

                                            _invalidPlay = false;
                                            Player = false;
                                            roundCount++;

                                            WinCondition();
                                            Console.Clear();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine($"\n\n0 is an invalid board location");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\nInput must be 1, 2, 3, 4, 5, 6, 7, 8 or 9"); // MAKE THIS UPDATE BASED ON AVAILABLE SPOTS?
                        }

                    } while (_invalidPlay);
                }
                else // False, so Player 2 - O turn.
                {
                    bool _invalidPlay = true;
                    do
                    {

                        UserBoard();
                        Console.Write("Where in the board do you wish to play Player 2 (O)? Type the number: ");
                        ConsoleKeyInfo _player1Input = Console.ReadKey();

                        if (char.IsDigit(_player1Input.KeyChar))
                        {
                            if (_player1Input.KeyChar.ToString() != "0")
                            {
                                for (int x = 0; x < 3; x++)
                                {
                                    for (int y = 0; y < 3; y++)
                                    {
                                        if (TicTacToeBoard[x, y].BoardPosition == _player1Input.KeyChar.ToString() && TicTacToeBoard[x, y].Used == true)
                                        {
                                            Console.Clear();
                                            Console.WriteLine($"Someone already played on the \"{_player1Input.KeyChar}\" board location.");
                                        }
                                        else if (TicTacToeBoard[x, y].BoardPosition == _player1Input.KeyChar.ToString() && TicTacToeBoard[x, y].Used == false)
                                        {
                                            TicTacToeBoard[x, y].Display = "O";
                                            TicTacToeBoard[x, y].Value = 10;
                                            TicTacToeBoard[x, y].Used = true;

                                            _invalidPlay = false;
                                            Player = true;
                                            roundCount++;
                                            WinCondition();
                                            Console.Clear();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine($"\n\n0 is an invalid board location");
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\nInput must be 1, 2, 3, 4, 5, 6, 7, 8 or 9"); // MAKE THIS UPDATE BASED ON AVAILABLE SPOTS?
                        }

                    } while (_invalidPlay);
                }
            } while (!GameOver);
        }
        static void GameEnd()
        {
            if (WinCondition() == true && roundCount == 9)
            {
                Console.WriteLine("It's a tie");
            }
            else
            {
                if (!Player)
                {
                    Console.WriteLine("PLAYER 1(X) WINS");
                }
                else if (Player)
                {
                    Console.WriteLine("PLAYER 2(O) WINS");
                }
            }
            UserBoard();
        }


        // Player Board Display

        static void UserBoard() // Actual Board Display.
        {

            Console.WriteLine($"\n\n {TicTacToeBoard[0, 0].Display} | {TicTacToeBoard[0, 1].Display} | {TicTacToeBoard[0, 2].Display} ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" {TicTacToeBoard[1, 0].Display} | {TicTacToeBoard[1, 1].Display} | {TicTacToeBoard[1, 2].Display} ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" {TicTacToeBoard[2, 0].Display} | {TicTacToeBoard[2, 1].Display} | {TicTacToeBoard[2, 2].Display} ");
            Console.WriteLine("\n");


        }
        static void UserBoardValues() // Board to Debug Values.
        {
            Console.WriteLine($" {TicTacToeBoard[0, 0].Value} | {TicTacToeBoard[0, 1].Value} | {TicTacToeBoard[0, 2].Value} ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" {TicTacToeBoard[1, 0].Value} | {TicTacToeBoard[1, 1].Value} | {TicTacToeBoard[1, 2].Value} ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" {TicTacToeBoard[2, 0].Value} | {TicTacToeBoard[2, 1].Value} | {TicTacToeBoard[2, 2].Value} ");
            Console.WriteLine("\n");
        }

        static void UserBoardPosition() // Board to Debug Values.
        {
            Console.WriteLine($" {TicTacToeBoard[0, 0].BoardPosition} | {TicTacToeBoard[0, 1].BoardPosition} | {TicTacToeBoard[0, 2].BoardPosition} ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" {TicTacToeBoard[1, 0].BoardPosition} | {TicTacToeBoard[1, 1].BoardPosition} | {TicTacToeBoard[1, 2].BoardPosition} ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" {TicTacToeBoard[2, 0].BoardPosition} | {TicTacToeBoard[2, 1].BoardPosition} | {TicTacToeBoard[2, 2].BoardPosition} ");
            Console.WriteLine("\n");
        }


        static void DebugAllBoards() // Both Boards at once for testing
        {
            UserBoard();
            UserBoardValues();
            UserBoardPosition();
        }

        static void InitializeArray() // Creates and populate the board Array.
        {
            int _display = 1;
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    TicTacToeBoard[x, y] = new TicTacToe(_display, _display, 0, false);
                    _display++;
                }
            }
        }
        static void Introduction() // Introduces the game, show or skips tutorial.
        {
            Console.WriteLine("Tic Tac Toe Game\n");
            Console.Write("Do you wish to read the tutorial? Type \"Y\" for Yes, \"N\" for No: ");
            ConsoleKeyInfo _userInput = Console.ReadKey();
            bool _invalidInput = true;

            while (_invalidInput)
            {
                if (_userInput.Key == ConsoleKey.Y)
                {
                    _invalidInput = false;
                    Tutorial();
                }
                else if (_userInput.Key == ConsoleKey.N)
                {
                    _invalidInput = false;
                    Console.Clear();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Tic Tac Toe Game\n");
                    Console.Write($"{_userInput.KeyChar} is not a valid option. \n\nDo you wish to read the tutorial? Type \"Y\" for Yes, \"N\" for No: ");                    
                    _userInput = Console.ReadKey();
                }

            }



        } 
        static void Tutorial()
        {
            Console.Clear();
            Console.WriteLine("Tic Tac Toe Game \n");
            Console.WriteLine("When it's your turn to play, type the number in the board where you want to place the X (Player1) or the O (Player2).\n");
            Console.WriteLine($" 1 | 2 | 3 ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" 4 | 5 | 6 ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" 7 | 8 | 9 ");
            Console.WriteLine("\n");
            Console.WriteLine("For example, if Player 1 (X) wishes to start at the center, type \"5\" and the board will change to: \n");
            Console.WriteLine($" 1 | 2 | 3 ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" 4 | X | 6 ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" 7 | 8 | 9 ");
            Console.WriteLine("\n");
            Console.WriteLine("Now, it will be Player 2 (O) turn, type for example \"8\" and the board will look like this:\n");
            Console.WriteLine($" 1 | 2 | 3 ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" 4 | X | 6 ");
            Console.WriteLine($"---|---|---");
            Console.WriteLine($" 7 | O | 9 ");
            Console.WriteLine("\n");
            Console.WriteLine("Press any key to continue with the game.");
            Console.ReadKey();
            Console.Clear();
        } // Skippable Tutorial
        static bool WinCondition() // clean up this a lot but this checks for the win condition.
        {
            if      (TicTacToeBoard[0, 0].Value + TicTacToeBoard[0, 1].Value + TicTacToeBoard[0, 2].Value == 3 || TicTacToeBoard[0, 0].Value + TicTacToeBoard[0, 1].Value + TicTacToeBoard[0, 2].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[1, 0].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[1, 2].Value == 3 || TicTacToeBoard[1, 0].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[1, 2].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[2, 0].Value + TicTacToeBoard[2, 1].Value + TicTacToeBoard[2, 2].Value == 3 || TicTacToeBoard[2, 0].Value + TicTacToeBoard[2, 1].Value + TicTacToeBoard[2, 2].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[0, 0].Value + TicTacToeBoard[1, 0].Value + TicTacToeBoard[2, 0].Value == 3 || TicTacToeBoard[0, 0].Value + TicTacToeBoard[1, 0].Value + TicTacToeBoard[2, 0].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[0, 1].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[2, 1].Value == 3 || TicTacToeBoard[0, 1].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[2, 1].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[0, 2].Value + TicTacToeBoard[1, 2].Value + TicTacToeBoard[2, 2].Value == 3 || TicTacToeBoard[0, 2].Value + TicTacToeBoard[1, 2].Value + TicTacToeBoard[2, 2].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[0, 0].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[2, 2].Value == 3 || TicTacToeBoard[0, 0].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[2, 2].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (TicTacToeBoard[0, 2].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[2, 0].Value == 3 || TicTacToeBoard[0, 2].Value + TicTacToeBoard[1, 1].Value + TicTacToeBoard[2, 0].Value == 30)
            {
                GameIsOver();
                return true;
            }
            else if (roundCount == 9)
            {
                GameIsOver();
                return true;
            }
            else
            {
                return false;
            }
        }

        static void GameIsOver()
        {
            GameOver = true;            
            
        } // only called by WinCondition();


    }
}
