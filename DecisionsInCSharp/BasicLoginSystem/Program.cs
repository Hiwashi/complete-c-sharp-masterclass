﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicLoginSystem
{
    class Program
    {
        // Static Variables created outside of the Main() and other Methods so they can be accessed and modified by any Method inside of the Program Class.
        static string _userUserName, _userUserPassword; // Username & Password temporage storage. Unsafe and temporary. 
        static bool _invalidUserName = true, _invalidUserPassword = true; // Default state for Username and Passwords are INVALID, once validation goes through, change to valid.
        static void Main(string[] args)
        {
            Register();
            LogIn();
        }

        public static void Register() // Register Method, creates a new default user and password.
        {
            Console.WriteLine("You will now be able to create a new account!\n"); // Greet user because we must be nice people.
            do
            {
                Console.Write("Username: ");
                _userUserName = Console.ReadLine(); // Gets the Username from the user
                if (_userUserName.Length > 10) // Check if the username is too long for some reason ¯\_(ツ)_/¯
                {
                    Console.WriteLine("Username has to be 10 charaters long or less. Try again.\n"); // If (_userUserName.Length > 10) returns TRUE, then the username is too long.

                }
                else
                {
                    _invalidUserName = false; 
                }
                
            } while (_invalidUserName);

            do // The same as the Username check, but this time it checks for a password with at least 6 digits and ONLY numbers / ulong, 0 to 18,446,744,073,709,551,615
            {
                Console.Write("Password: ");
                _userUserPassword = Console.ReadLine();
                bool _IsPasswordNumbersOnly = ulong.TryParse(_userUserPassword, out ulong result); // Check if password only contains numbers (integer).
                
                if (_userUserPassword.Length < 6)
                {
                    Console.WriteLine("\nPassword has to have 6 or more characteres. Try again. \n");
                }
                else if (_userUserPassword.Length > 18)
                {
                    Console.WriteLine("\nPassword cannot have more than 18 characteres. Try again.\n");
                }
                else if (!_IsPasswordNumbersOnly) // if it returns FALSE
                {
                    Console.WriteLine("\nPassword can only contain numbers\n");
                }
                else
                {
                    _invalidUserPassword = false;
                }
            } while (_invalidUserPassword);

            Console.WriteLine("\nRegistration Successful!");


        
        }
        public static void LogIn()
        {
            do
            {
                Console.WriteLine("\nType a valid Username and Password to Login\n");
                Console.Write("Username: ");
                string username = Console.ReadLine();
                Console.Write("Password: ");
                string password = Console.ReadLine();

                if (username.Equals(_userUserName) && password.Equals(_userUserPassword))
                {
                    Console.WriteLine("\nLogin susscessful!");
                    break;

                }
            } while (true);
            Console.Read();
        }// Login Method
    }
}
