﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _43Challenge
{
    class Program
    {
        static string highScorePlayer;
        static int highScore;
        static void Main(string[] args)
        {
            /* Create an application with a Score, Highscore and a HighScorePlayer
             * Create a Method which has two parameters, one for the score and one for the playerName
             * When that method is called, it should be checked if the score of the player is higher than the highscore
             * If so, "New highscore is + " score" and in another line "New highscore holder is " + playerName - should be written onto the console
             * if not "The old highscore of " + highscore + " could not be broken and is still held by " + highscorePlayer.
             * Consider which variables are required globally and which ones locally.
             */

            Console.Write("Enter the player name: ");
            string playerName = Console.ReadLine();
            Console.Write("Enter the player score: ");
            int score = int.Parse(Console.ReadLine());

            PlayerScore(score, playerName);

            PlayerScore(80, "Maria");
            PlayerScore(310, "Steve");
            PlayerScore(138, "Carlos");
            PlayerScore(311, "Bird");

            Console.Read();
            
        }

        public static void PlayerScore(int playerScore, string playerName)
        {
            if (playerScore > highScore)
            {
                highScorePlayer = playerName;
                highScore = playerScore;
                Console.WriteLine($"New highscore holder is {highScorePlayer} with a score of {highScore}!");

            }
            else
            {
                Console.WriteLine($"The old highschore of {highScore} could not be broken by {playerName} and is still held by {highScorePlayer}");
            }
        }
    }
}
