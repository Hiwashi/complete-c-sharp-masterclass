﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternary_Operator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Condition ? First_Expression : Second_Expression;
            // 
            // Condition has to be either true or false
            // The conditional operator is right - associative
            //
            //
            //
            //

            // in celcius
            int temperature = -5;
            string stateOfMatter;

            if (temperature < 0)
            {
                stateOfMatter = "solid";
            }
            else
            {
                stateOfMatter = "liquid";               
            }

            Console.WriteLine($"State of matter is {stateOfMatter}");

            temperature += 30;

            //in short

            stateOfMatter = temperature < 0 ? "solid" : "liquid";

            Console.WriteLine($"State of matter is {stateOfMatter}");

            // challenge - add the gas state of liquid

            temperature = 110;
            stateOfMatter = temperature < 0 ? "solid" : temperature > 100 ? "gas" : "liquid";
            Console.WriteLine($"State of matter is {stateOfMatter}");

            temperature = 40;
            stateOfMatter = temperature < 0 ? "solid" : temperature > 100 ? "gas" : "liquid";
            Console.WriteLine($"State of matter is {stateOfMatter}");

            temperature = -5;
            stateOfMatter = temperature < 0 ? "solid" : temperature > 100 ? "gas" : "liquid";
            Console.WriteLine($"State of matter is {stateOfMatter}");

            Console.Read();

        }
    }
}
